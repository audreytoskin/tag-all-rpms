function koji-scm-tag-all --description "Tag all the release references for this RPM spec"

    function __koji-scm-tag-help-message --description "Display command line syntax for the main function"
        echo "Usage:"
        echo "  (`cd` into your local RPM spec repository, then...)"
        echo "  ↪ koji-scm-tag-all -h|--help"
        echo "  ↪ koji-scm-tag-all APP_NAME PKG_NAME USERNAME --dry-run"
        echo "  ↪ koji-scm-tag-all APP_NAME PKG_NAME USERNAME"
        echo
        echo "Arguments:"
        echo "  APP_NAME is the brand name, with proper capitalization, etc."
        echo "  PKG_NAME is the RPM package name, as written in the Fedora/EPEL repos."
        echo "  USERNAME is the Koji handle of the person who created the package builds."
        echo
        echo "Options:"
        echo "  -h|--help    Display this help message."
        echo "  --dry-run    When appended to the end of the command line, scan and display the gathered info without actually applying the git tags."
    end

    if test "x$argv" = "x--help"  -o  "x$argv" = "x-h"
        __koji-scm-tag-help-message
        return 0
    end

    # Local `set` here first, because while most other variables are necessarily assigned values which would override upper scopes, $dry_run is only set if the --dry-run option flag is provided on the command line; so when unset, it could possibly inherit a higher exported $dry_run value.
    set --local dry_run
    if test "x$argv[ 4 ]" = "x--dry-run"
        # We'll test and run the dry-run condition later, and avoid actually creating git tags.
        set --export dry_run "true"
        echo "DRY RUN ONLY, not applying any git tags."
    end
    if test -n "$dry_run"
        if test ( count $argv ) -ne 4
            echo "Wrong number of arguments" >&2
            echo >&2
            __koji-scm-tag-help-message >&2
            return 1
        end
    else if test ( count $argv ) -ne 3
        echo "Wrong number of arguments" >&2
        echo >&2
        __koji-scm-tag-help-message >&2
        return 1
    end

    set --export app_name "$argv[ 1 ]"
    set --export pkg_name "$argv[ 2 ]"
    set --export username "$argv[ 3 ]"


    # Temporary folder to contain generated helper files -- queried Koji logs, an interim Fish script, etc.
    set tmpdir "./__koji-scm-tag-all-"( uuidgen )
    mkdir -p "$tmpdir"


    # We look up the information needed for the function arguments here in a moment.
    function __git-tag-cmd-generator --description "Generate and execute the git-tag command with signed tag annotation"
        # Usage: __git-tag-cmd-generator VERSION_RELEASE COMMIT_HASH
        #
        #     where:
        #       VERSION_RELEASE is the package version-release number (without "v" prefix, arch,
        #         or Fedora/EPEL branch, formatted like `X.Y.Z-R` or `X.Y.Z-R.DATE.UPSTREAM_COMMIT`
        #       COMMIT_HASH is the spec repo's commit hash corresponding to that release.

        set rpm_version_release "$argv[ 1 ]"
        set rpm_commit          "$argv[ 2 ]"
        set upstream_version    ( echo "$rpm_version_release" | sed -E 's/([[:alnum:].]+)-[[:alnum:].]+.*/\1/g' )
        set rpm_release         ( echo "$rpm_version_release" | sed -E 's/[[:alnum:].]+-([[:alnum:].]+).*/\1/g' )
        set --local rpm_release_base
        set --local upstream_commit
        switch "$rpm_release"
            case '*.*.*'
                set rpm_release_base  ( echo "$rpm_release" | sed -E 's/^([[:digit:]]+)\..+/\1/g' )
                set upstream_commit   ( echo "$rpm_release" | sed -E 's/[[:digit:]]+\.[[:digit:]]+\.(git|hg)?([[:alnum:]]+)/\2/g' )
        end

        if test -n "$dry_run"
            echo
            echo "RPM version-release: $rpm_version_release"
            echo "RPM commit: $rpm_commit"
            echo "App version: $upstream_version"
            echo "RPM release: $rpm_release"
            if test -n "$upstream_commit"
                echo "RPM base release: $rpm_release_base"
                echo "Upstream commit: $upstream_commit"
            end
            return 0
        else
            if test -n "$upstream_commit"
                git tag --sign "v$rpm_version_release" "$rpm_commit" --message="$app_name version $upstream_version (plus upstream commit $upstream_commit), RPM release $rpm_release_base"
            else
                git tag --sign "v$rpm_version_release" "$rpm_commit" --message="$app_name version $upstream_version, RPM release $rpm_release"
            end
        end
    end


    # First we need to look up all past releases, and the corresponding git commits.
    echo "Searching... (Duplicates are likely at this phase, but we'll filter them out in a moment.)"
    # Query Koji for *all* my tasks (or tasks by my comaintainers), filter for successful builds of the package we're currently working on, and save to a log file.
    koji list-history --editor="$username" --package="$pkg_name" > "$tmpdir/koji-history.log"
    if test ! -s "$tmpdir/koji-history.log"
        echo "Koji found no matches for $username building $pkg_name." >&2
        # Comment this line to save the temporary files for debugging.
        rm -rf "$tmpdir"
        return 2
    end

    # Extract just the NVR build names.
    # Sometimes I accidentally published builds with the upstream commit put after the Fedora/EPEL branch suffix...
    grep -E --only-matching $pkg_name'-[[:alnum:].]+-[[:alnum:].]+\.(fc|el)[[:digit:]]+(\.[[:digit:]]+\.[[:alnum:]]+)?'  "$tmpdir/koji-history.log" > "$tmpdir/koji-builds.txt"

    # Start building the script we will use to tag commits in a batch...
    # First generate commands in a plain text file.
    for build in ( cat "$tmpdir/koji-builds.txt" )
        set version_release ( echo "$build" | sed -E "s/$pkg_name-//g" | sed -E 's/\.(fc|el)[[:digit:]]+//g' )
        set commit ( fedpkg gitbuildhash "$build" )
        # Proceed with adding this __git-tag-cmd-generator line to script, only if fedpkg found a matching gitbuildhash.
        and begin
            echo "Found: $version_release -- $commit"
            echo "__git-tag-cmd-generator \"$version_release\" \"$commit\"" >> "$tmpdir/unfiltered-git-functions.txt"
        end
        or begin
            echo "fedpkg could not find a git commit that matches $build" >&2
            return 3
        end
    end
    # Prepare the actual script file.
    echo '#!'( which fish ) > "$tmpdir/git-tags.fish"
    chmod +x "$tmpdir/git-tags.fish"
    # Remove duplicates caused by the same version-release getting built for different Fedora/EPEL branches, and add to the final script.
    sort --unique "$tmpdir/unfiltered-git-functions.txt" >> "$tmpdir/git-tags.fish"

    echo
    # Simply executing the generated script doesn't seem to inherit the $pkg_name variable or __git-tag-cmd-generator function... so we're evaluating instead.
    for cmd in ( cat "$tmpdir/git-tags.fish" )
        eval "$cmd"
    end

    # Comment this line to save the temporary files for debugging.
    rm -rf "$tmpdir"
end
